#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

void passString(char * fname);

int main()
{
    // gets file name as a input
    char inpBuff[1000];
    printf("Please enter your filename: ");
    int n = scanf("%s",inpBuff);
    if (n==0)
    {
        printf("Error: no filename given, now exiting");
    }
    else
    {
        passString(inpBuff);
    }
}
void passString(char * fname)
{
        // need to get 3 things, lines, characters, and words
        // lines are denoted by /n +1 (for first line)
        // characters are everything including /n
        // words are alphanumeric char followed by a space = +1 word
        
        // given that no line is more than 1k char long we can do a for loop 
        
        int lines=0,words=0,characters=0;
        
        // open file in read mode
        FILE *fp = fopen(fname,"r");
        int c;
        int lastC;
        while (1)
        {
            c = fgetc(fp);
            if(feof(fp))
            {
                break;
            }
            characters++;
            if (c == '\n')
            {
                lines++;
            }
            if (isspace(c)|| c == '\n')
            {
                if (isalpha(lastC))
                {
                    words++;
                }
            }
            lastC = c;
            
        }
        if (isalpha(lastC))
            {
                words++;
            }
        lines++; // this will add a line to the lines var since a end line isnt counted on a single line file
        printf("Lines: %d \nWords: %d \nCharacters: %d \n",lines, words, characters);
}