#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

void reverseFileOtp(char * filenameInp, char * filenameOtp);
void reverseStdOtp(char * filenameInp);

int main()
{
    // accepts two inputs, filename Inp and filename Otp (if no filename Otp is given then write to stdout)
    char inpBuff[1000];
    char otpBuff[1000];
    printf("PLease enter the input filename ");
    int n = scanf("%s",&inpBuff);
    printf("PLease enter the output filename ");
    int x = scanf("%s",&otpBuff);
    if (n==1 && x==1)
    {
        reverseFileOtp(inpBuff,otpBuff);
    }
    else if ((n==1 && x==0)||(n==0 && x==1))
    {
        if(n==1)
        {
            reverseStdOtp(inpBuff);
        }
        else
        {
            reverseStdOtp(otpBuff);
        }
    }
    else
    {
        printf("Error: no filename given, now exiting");
    }
}
void reverseStdOtp(char * filenameInp)
{  
    FILE *fpr = fopen(filenameInp,"r");
    
    char lineBuffer[10000];
    char whiteSpaceBuffer[1000];
    int c; // holds current char to add to array or test for EOL
    
    // if statement will determine if written to output file or stdout
    
    
     // holds the indexed value 
    while (1)
    {
        memset(lineBuffer, 0, 10000);
        memset(whiteSpaceBuffer, 0, 1000);
        if(feof(fpr))
        {
            break;
        }
        
        int i=0;
        int wInd=0;
        int lastC = ' ';
        do
        {
            c = fgetc(fpr);
            
            // this will add the spaces to whiteSpaceBuffer arr instead of lineBuffer
            if (c=='\t' || (isspace(c) && isspace(lastC)))
            {
                whiteSpaceBuffer[wInd] = c;
                wInd++;
            }
            else
            {
                if(feof(fpr))
                {
                    break;
                }
                if (c == '\n')
                {
                    break;
                }
                lineBuffer[i]=c;
                i++;
            }
            lastC = c;
        }while (1);
        if (strlen(whiteSpaceBuffer) > 0)
        {
            for (int index = 0; index < strlen(whiteSpaceBuffer); index++)
            {
                printf("%c",whiteSpaceBuffer[index]);
            }
        }
        for (int index = strlen(lineBuffer)-1;index > -1; index--)
        {
            printf("%c",lineBuffer[index]);
        }
        printf("\n");
        
    }
    
}
void reverseFileOtp(char * filenameInp, char * filenameOtp)
{  
    FILE *fpr = fopen(filenameInp,"r");
    FILE *fpw = fopen(filenameOtp,"w");
    
    char lineBuffer[10000];
    char whiteSpaceBuffer[1000];
    int c; // holds current char to add to array or test for EOL
    
    // if statement will determine if written to output file or stdout
    
    
     // holds the indexed value 
    while (1)
    {
        memset(lineBuffer, 0, 10000);
        memset(whiteSpaceBuffer, 0, 1000);
        if(feof(fpr))
        {
            break;
        }
        
        int i=0;
        int wInd=0;
        int lastC = ' ';
        do
        {
            c = fgetc(fpr);
            
            // this will add the spaces to whiteSpaceBuffer arr instead of lineBuffer
            if (c=='\t' || (isspace(c) && isspace(lastC)))
            {
                whiteSpaceBuffer[wInd] = c;
                wInd++;
            }
            else
            {
                if(feof(fpr))
                {
                    break;
                }
                if (c == '\n')
                {
                    break;
                }
                lineBuffer[i]=c;
                i++;
            }
            lastC = c;
        }while (1);
        if (strlen(whiteSpaceBuffer) > 0)
        {
            for (int index = 0; index < strlen(whiteSpaceBuffer); index++)
            {
                fputc(whiteSpaceBuffer[index], fpw);
            }
        }
        for (int index = strlen(lineBuffer)-1;index > -1; index--)
        {
            fputc(lineBuffer[index],fpw);
        }
        fputc('\n',fpw);
    }
    
}